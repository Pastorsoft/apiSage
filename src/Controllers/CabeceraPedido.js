const { sqlConfig, sqlConfigERP } = require("../../conexion");
const moment = require("moment");

let resultado = [];

async function CabeceraPedidos(req, res) {
  let FechaInicio = "2021-10-10";
  let FechaFinal = moment().format("YYYY-MM-DD");

  const result = await sqlConfig.executeQuery(
    "select * from CabeceraPedidoCliente where estado=0 and FechaPedido between @fechaInicio and @fechaFinal  ",
    [
      { name: "fechaInicio", type: "varchar", value: FechaInicio },
      { name: "fechaFinal", type: "varchar", value: FechaFinal },
    ]
  );

  res.send(result.data[0]);
}

module.exports = {
  CabeceraPedidos,
};
