const express = require("express");
const Controllers = require("../Controllers/CabeceraPedido");
const router = express.Router();

router.get("/", Controllers.CabeceraPedidos);

module.exports = router;
