const express = require("express");
const app = express();
const port = 3000;

const rutas = require("./src/Routes/Rutas");

app.use(express.json());

app.use("/", rutas);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
