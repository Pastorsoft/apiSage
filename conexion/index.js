const Config = require("../config");

const sqlConfig = new (require("rest-mssql-nodejs"))({
  user: Config.Usuario,
  password: Config.Password,
  database: Config.BaseDatos,
  server: Config.Servidor,
});
const sqlConfigERP = new (require("rest-mssql-nodejs"))({
  user: Config.ERPUsuario,
  password: Config.ERPPassword,
  database: Config.ERPBaseDatos,
  server: Config.ERPServidor,
});

module.exports = {
  sqlConfig,
  sqlConfigERP,
};
